package evertz.evertz_management.application_db_calls;
import evertz.evertz_management.application_server.logger;
import java.sql.Timestamp;
import java.util.Date;


public class GenerateTimeStamp {

	public String getTimeStap(){
		
		try {
			
			 Date date= new Date();
			 long time = date.getTime();
			 Timestamp ts = new Timestamp(time);
			 logger.info("TimeStamp: "+ts);
			 return ts.toString();
			
		} catch (Exception e){
			logger.error(e);
			return "false";
		}

	}
}
