package evertz.evertz_management.application_db_calls;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import evertz.evertz_management.application_server.SystemConfig;
import evertz.evertz_management.application_server.logger;

public class ConnectToDatabase {

	private static String dbServerIp = null;
	private static String databaseName = null;
	private static String dbUsername = null;
	private static String dbPassword = null;
	private static String dbPort = null;
	
	public ConnectToDatabase() throws Exception {

		try {
			dbServerIp = SystemConfig.getSystemConfig("SQLServerIp");
			databaseName = SystemConfig.getSystemConfig("DatabaseName");
			dbUsername = SystemConfig.getSystemConfig("SQLUserName");
			dbPassword = SystemConfig.getSystemConfig("SQLPassword");
			dbPort = SystemConfig.getSystemConfig("SQLPort");

		} catch (Exception e) {

			logger.error(e);
			throw new Exception(e.getMessage());
		}

	}
	
	public Connection getConnection() throws Exception {

		Connection con = null;

		try {

			String dbURL = "jdbc:mysql://" + dbServerIp + ":" + dbPort + "/" + databaseName
					+ "?useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";

			Class.forName("com.mysql.cj.jdbc.Driver");

			con = DriverManager.getConnection(dbURL, dbUsername, dbPassword);

			return con;

		} catch (Exception e) {

			logger.error(e);
			throw new Exception(e.getMessage());
		}
	}
	
	public int updateData(Connection con, String dmlQuery) throws Exception {

		try {

			Statement st = con.createStatement();
			int ur = st.executeUpdate(dmlQuery);
			return ur;

		} catch (Exception e) {

			logger.error(e);
			throw new Exception(e.getMessage());
		}

	}

	public ResultSet selectData(Connection con, String projectionQuery) throws Exception {

		try {

			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(projectionQuery);

			return rs;

		} catch (Exception e) {

			logger.error(e);
			throw new Exception(e.getMessage());
		}
	}
	
	public Connection getConnectionToServer(String serverIp) throws Exception {
		Connection con = null;

		try {

			String dbURL = "jdbc:mysql://" + serverIp + ":" + dbPort + "/" + databaseName
					+ "?useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";

			Class.forName("com.mysql.cj.jdbc.Driver");

			con = DriverManager.getConnection(dbURL, dbUsername, dbPassword);

			return con;

		} catch (Exception e) {

			logger.error(e);
			throw new Exception(e.getMessage());
		}
	}
	
	
}
