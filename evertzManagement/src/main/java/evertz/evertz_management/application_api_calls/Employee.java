package evertz.evertz_management.application_api_calls;

import java.sql.Connection;
import java.sql.ResultSet;

import org.json.simple.JSONObject;

import evertz.evertz_management.application_db_calls.ConnectToDatabase;
import evertz.evertz_management.application_server.logger;

public class Employee {
	
	private JSONObject parameterList;
	public Employee(JSONObject parameterList) {
		this.parameterList = parameterList;
	}

	@SuppressWarnings("unchecked")
	public JSONObject addEmployeeToServer() throws Exception {
		
		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		String insertQuery = null;
		String selectQuery = null;
		ResultSet rs = null;
		String empId = "";
		String firstName = (String) this.parameterList.get("FirstName");
		String middleName = (String) this.parameterList.get("MiddleName");
		String lastName = (String) this.parameterList.get("LastName");
		String gender = (String) this.parameterList.get("GenderId");
		String doj = (String) this.parameterList.get("DateOfJoin");
		String designation = (String) this.parameterList.get("DesignationId");
		String team = (String) this.parameterList.get("TeamId");
		String region = (String) this.parameterList.get("RegionId");
		String dept = (String) this.parameterList.get("DepartmentId");
		String profilePicLoc = (String) this.parameterList.get("DefaultPhotoPath");
		String userAccess = (String) this.parameterList.get("UserAccess");
		String pwd = (String) this.parameterList.get("Password");
		
		try {
			
			selectQuery = "select EMPLOYEE_CODE from employee order by ID desc limit 1";
			logger.info(selectQuery);
			rs = DBConnect.selectData(con, selectQuery);
			while (rs.next()) {
				empId = rs.getString("EMPLOYEE_CODE");
			}
			String lastEmpId = empId.substring(3);
			int newEmpId = Integer.parseInt(lastEmpId) + 1;
			empId = "EID" + String.valueOf(newEmpId);
			
			insertQuery = "INSERT INTO employee (`EMPLOYEE_CODE`, `FIRTS_NAME`, `MIDDLE_NAME`, `LAST_NAME`, `GENDER_ID`, `DATE_OF_JOIN`, `DESIGNATION_ID`, `DEPARTMENT_ID`, `TEAM_ID`, `REGION_ID`, `PROFILE_PIC_BASE64`)"
					+ "values(\""+ empId + "\",\""+ firstName +"\",\""+ middleName +"\",\""+ lastName +"\",\""+ gender +"\",\""+ doj +"\",\""+ designation +"\",\""+ dept +"\",\""+ team +"\",\""+ region +"\",\""+ profilePicLoc +"\");";
			logger.info(insertQuery);
			int rowsAffected = DBConnect.updateData(con, insertQuery);
			if(rowsAffected == 1) {
				//success
				String eid = null;
				selectQuery = "select ID from employee where EMPLOYEE_CODE=\""+ empId + "\" limit 1;";
				logger.info(selectQuery);
				rs = DBConnect.selectData(con, selectQuery);

				while (rs.next()) {
					eid = rs.getString("ID");
				}
				
				insertQuery = "INSERT INTO user (`EMPLOYEE_ID`, `USERNAME`, `PASSWORD`, `IS_ACTIVE`, `USER_ACCESS_ID`, `IS_NEW_USER`)"
						+ "values(\""+ eid +"\",\""+ empId +"\",\""+ pwd +"\",\""+ 1 +"\",\""+ userAccess +"\",\""+ 1 +"\");";
				logger.info(insertQuery);
				rowsAffected = DBConnect.updateData(con, insertQuery);
				if(rowsAffected == 1) {
					//new user created
					con.close();
					logger.info("Emplyee Added and Access provided");
					responseParameterList.put("EmployeeCode", empId);
					returnResponseObject.put("Success", true);
					returnResponseObject.put("ParameterList", responseParameterList);
					return returnResponseObject;
					
				}else {
					//failed to create user, remove data from emp table
					String deleteQuery = "delete from employee where ID=\""+ eid + "\";";
					logger.info(deleteQuery);
					rowsAffected = DBConnect.updateData(con, deleteQuery);
					if(rowsAffected == 1) {
						//deleted employee from employee table
						throw new Exception("Failed to Add Employee to Server");
					}else {
						throw new Exception("Employee added, But failed to provide access");
					}
				}
				
			}else {
				//failed to add employee
				throw new Exception("Failed to Add Employee to Server");
			}
			
		}catch (Exception e) {
			logger.error(e.getMessage());
			con.close();
			responseParameterList.put("Reason",e.toString());
			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);
			return returnResponseObject;
		}
	}
}
