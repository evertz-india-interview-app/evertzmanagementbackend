package evertz.evertz_management.application_api_calls;

import java.sql.Connection;
import java.sql.ResultSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import evertz.evertz_management.application_db_calls.ConnectToDatabase;
import evertz.evertz_management.application_server.logger;

public class Function {

	private JSONObject parameterList;
	public Function(JSONObject parameterList) {
		this.parameterList = parameterList;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getTools() throws Exception {
		
		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		String selectQuery = null;
		ResultSet rs = null;
		String userAccess = (String) this.parameterList.get("UserAccess");
		JSONObject returnToolsOptionList = new JSONObject();
		try {
			
			selectQuery = "select fl.SORT_ORDER,f.FUNCTION_NAME,f.FUNCTION_CODE from function_lnk fl\r\n" + 
					"inner join function f on f.ID = fl.FUNCTION_ID\r\n" + 
					"where fl.USER_ACCESS_ID = "+ userAccess +" and fl.IS_ACTIVE = 1;";
			logger.info(selectQuery);
			rs = DBConnect.selectData(con, selectQuery);
			JSONArray toolsList = new JSONArray();
			while (rs.next()) {
				JSONObject eachData = new JSONObject();
				eachData.put("SortOrder", rs.getInt(1));
				eachData.put("FunctionName", rs.getString(2));
				eachData.put("FunctionCode", rs.getInt(3));

				toolsList.add(eachData);
			}
			con.close();
			returnToolsOptionList.put("FunctionList",toolsList);
			returnToolsOptionList.put("UserAccess",userAccess);
			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", returnToolsOptionList);
			return returnResponseObject;
			
		}catch (Exception e) {
			logger.error(e.getMessage());
			con.close();
			responseParameterList.put("Reason",e.toString());
			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);
			return returnResponseObject;
		}
		
	}

}
