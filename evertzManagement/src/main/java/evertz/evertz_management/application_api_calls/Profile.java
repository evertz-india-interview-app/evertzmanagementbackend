package evertz.evertz_management.application_api_calls;

import java.util.Base64;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import java.sql.Connection;
import java.sql.ResultSet;
import org.json.simple.JSONObject;

import evertz.evertz_management.application_db_calls.ConnectToDatabase;
import evertz.evertz_management.application_server.logger;

public class Profile {
	
	private JSONObject parameterList;
	public Profile(JSONObject parameterList) {
		this.parameterList = parameterList;
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	public JSONObject getProfilePic() throws Exception {
		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		String selectQuery = null;
		ResultSet rs = null;
		String employeeCode = (String) this.parameterList.get("EmployeeCode");
		String profileBase64 = null;
		try {
			
			selectQuery = "select PROFILE_PIC from employee where(EMPLOYEE_CODE='"+employeeCode+"' and ID <>0) limit 1;";
			//logger.info(selectQuery);
			rs = DBConnect.selectData(con, selectQuery);
			while (rs.next()) {
				profileBase64 = rs.getString("PROFILE_PIC");
			}
			
		    byte[] input = Base64.getDecoder().decode(profileBase64);
		    Inflater decompresser = new Inflater();
		    decompresser.setInput(input);
		    byte[] result = profileBase64.getBytes();
		    int resultLength = decompresser.inflate(result);
		    decompresser.end();

		    String output64 = new String(result, 0, resultLength, "UTF-8");
		    
			responseParameterList.put("EmployeeCode",employeeCode);
			responseParameterList.put("ProfilePicBase64",output64);
			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", responseParameterList);
			return returnResponseObject;
			
		}catch (Exception e) {
			logger.error(e.getMessage());
			con.close();
			responseParameterList.put("EmployeeCode",employeeCode);
			responseParameterList.put("Reason",e.toString());
			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);
			return returnResponseObject;
		}
		
		
	}
	
	
	@SuppressWarnings({ "unchecked", "unused" })
	public JSONObject updateProfilePic() throws Exception {
		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		String updateQuery = null;
		ResultSet rs = null;
		String employeeCode = (String) this.parameterList.get("EmployeeCode");
		String profileBase64 = (String) this.parameterList.get("ProfilePicBase64");

		try {
			
			byte[] input = profileBase64.getBytes("UTF-8");
		    byte[] output = new byte[input.length];
		    Deflater compresser = new Deflater();
		    compresser.setInput(input);
		    compresser.finish();
		    int compressedDataLength = compresser.deflate(output);
		    compresser.end();
		    String compressed64 = new String(Base64.getEncoder().encode(output));
		    
			updateQuery = "update employee set PROFILE_PIC = '"+ compressed64 +"' where(EMPLOYEE_CODE='"+ employeeCode +"' and ID <>0);";
			logger.info(updateQuery);
			int rowsAffected = DBConnect.updateData(con, updateQuery);
			if(rowsAffected == 1) {
				con.close();
				responseParameterList.put("EmployeeCode",employeeCode);
				responseParameterList.put("Reason","Profile Updated Sucessfully.");
				returnResponseObject.put("Success", true);
				returnResponseObject.put("ParameterList", responseParameterList);
				return returnResponseObject;
			}else {
				
				throw new Exception("Server failed to Update profile pic");
				
			}
			

			
		}catch (Exception e) {
			logger.error(e.getMessage());
			con.close();
			responseParameterList.put("EmployeeCode",employeeCode);
			responseParameterList.put("Reason",e.toString());
			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);
			return returnResponseObject;
		}
				
		
	}

	private Exception String(int i) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	

}
