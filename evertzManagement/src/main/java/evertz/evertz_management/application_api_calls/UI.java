package evertz.evertz_management.application_api_calls;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import evertz.evertz_management.application_db_calls.ConnectToDatabase;
import evertz.evertz_management.application_server.logger;


public class UI {

	private JSONObject uiParameterList;
	public UI(JSONObject uiParameterList) {
		this.uiParameterList = uiParameterList;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	public JSONObject getDropDownList() throws Exception {

		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();

		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();

		ResultSet rs = null;
		JSONObject returnDropdownValues = new JSONObject();
		JSONArray dropdownFieldValue = new JSONArray();
		
		String selectQuery = "";

		try {

			dropdownFieldValue =  (JSONArray) this.uiParameterList.get("UIDropdownField");

			for(int i=0; i<dropdownFieldValue.size(); i++) {
				String uiListToGet = dropdownFieldValue.get(i).toString();
				
				if(uiListToGet.equals( "Gender")) {
					
					selectQuery = "select * from gender";
					logger.info(selectQuery);
					rs = DBConnect.selectData(con, selectQuery);
					JSONArray dropdownValues = new JSONArray();
					while (rs.next()) {

						JSONObject eachData = new JSONObject();
						eachData.put("Id", rs.getInt(1));
						eachData.put("Name", rs.getString(2));

						dropdownValues.add(eachData);

					}
					returnDropdownValues.put("GenderList",dropdownValues);
					
				}else if(uiListToGet.equals("Designation")){
					
					selectQuery = "select * from designation";
					logger.info(selectQuery);
					rs = DBConnect.selectData(con, selectQuery);
					JSONArray dropdownValues = new JSONArray();
					while (rs.next()) {

						JSONObject eachData = new JSONObject();
						eachData.put("Id", rs.getInt(1));
						eachData.put("Name", rs.getString(2));

						dropdownValues.add(eachData);

					}
					returnDropdownValues.put("DesignationList",dropdownValues);
					
				}else if(uiListToGet.equals("Team")){
					
					selectQuery = "select * from team";
					logger.info(selectQuery);
					rs = DBConnect.selectData(con, selectQuery);
					JSONArray dropdownValues = new JSONArray();
					while (rs.next()) {

						JSONObject eachData = new JSONObject();
						eachData.put("Id", rs.getInt(1));
						eachData.put("Name", rs.getString(2));

						dropdownValues.add(eachData);

					}
					returnDropdownValues.put("TeamList",dropdownValues);
					
				}else if(uiListToGet.equals("Region")){
					
					selectQuery = "select * from region";
					logger.info(selectQuery);
					rs = DBConnect.selectData(con, selectQuery);
					JSONArray dropdownValues = new JSONArray();
					while (rs.next()) {

						JSONObject eachData = new JSONObject();
						eachData.put("Id", rs.getInt(1));
						eachData.put("Name", rs.getString(2));

						dropdownValues.add(eachData);

					}
					returnDropdownValues.put("RegionList",dropdownValues);
					
				}else if(uiListToGet.equals("Department")){
					
					selectQuery = "select * from department";
					logger.info(selectQuery);
					rs = DBConnect.selectData(con, selectQuery);
					JSONArray dropdownValues = new JSONArray();
					while (rs.next()) {

						JSONObject eachData = new JSONObject();
						eachData.put("Id", rs.getInt(1));
						eachData.put("Name", rs.getString(2));

						dropdownValues.add(eachData);

					}
					returnDropdownValues.put("DepartmentList",dropdownValues);
					
				}else if(uiListToGet.equals("UserAccess")){
					
					selectQuery = "select * from user_access";
					logger.info(selectQuery);
					rs = DBConnect.selectData(con, selectQuery);
					JSONArray dropdownValues = new JSONArray();
					while (rs.next()) {

						JSONObject eachData = new JSONObject();
						eachData.put("Id", rs.getInt(1));
						eachData.put("Name", rs.getString(2));

						dropdownValues.add(eachData);

					}
					returnDropdownValues.put("UserAccesstList",dropdownValues);
					
				}
	
			}
			
			returnDropdownValues.put("UIDropdownField",dropdownFieldValue);
			returnResponseObject.put("Success", true);
			returnResponseObject.put("ParameterList", returnDropdownValues);
			//logger.info(returnResponseObject.toString());
			con.close();
			return returnResponseObject;
			
		} catch (Exception e) {

			logger.error(e);
			responseParameterList.put("Reason", e.getMessage());
			returnDropdownValues.put("UIDropdownField",dropdownFieldValue);
			returnResponseObject.put("Success", false);
			con.close();
			return returnDropdownValues;
		}
		
	}
}
