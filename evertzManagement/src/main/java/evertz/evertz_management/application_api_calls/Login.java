package evertz.evertz_management.application_api_calls;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Base64;
import java.util.zip.Inflater;

import org.json.simple.JSONObject;

import evertz.evertz_management.application_db_calls.ConnectToDatabase;
import evertz.evertz_management.application_db_calls.GenerateTimeStamp;
import evertz.evertz_management.application_server.logger;

public class Login {

	private JSONObject loginParameterList;
	JSONObject reasonAndStatusObject = new JSONObject();
	
	public Login(JSONObject loginParameterList) {

		this.loginParameterList = loginParameterList;

		}
	
	@SuppressWarnings("unchecked")
	public JSONObject loginToServer() throws Exception {
		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		try {
			ResultSet rs = null;
			String selectQuery = null;
			String updateQuery = null;
			String passwordOnDB = null;
			String loginTime = "";
			int isActiveUser = 0;
			int userAccess = 0;
			int newUser = 0;
			String firstName = null;
			String secondName = null;
			String lastName = null;
			int empId = 0;
			String userName = (String) this.loginParameterList.get("UserName");
			String password = (String) this.loginParameterList.get("Password");
			//String profilePic = null;
			
			selectQuery = "select u.PASSWORD,u.IS_ACTIVE,u.USER_ACCESS_ID,u.IS_NEW_USER,e.ID as EMPLOYEE_ID,e.FIRTS_NAME,e.MIDDLE_NAME,e.LAST_NAME from user u\r\n" + 
					"inner join employee e on e.ID = u.EMPLOYEE_ID\r\n" + 
					" where u.USERNAME='"+ userName +"';";
			logger.info(selectQuery);
			rs = DBConnect.selectData(con, selectQuery);
			while (rs.next()) {
				passwordOnDB = rs.getString("PASSWORD");
				isActiveUser = rs.getInt("IS_ACTIVE");
				userAccess = rs.getInt("USER_ACCESS_ID");
				newUser = rs.getInt("IS_NEW_USER");
				firstName = rs.getString("FIRTS_NAME");
				secondName = rs.getString("MIDDLE_NAME");
				lastName = rs.getString("LAST_NAME");
				empId = rs.getInt("EMPLOYEE_ID");
				//profilePic = rs.getString("PROFILE_PIC");
			}
			
		    /*byte[] input = Base64.getDecoder().decode(profilePic);
		    Inflater decompresser = new Inflater();
		    decompresser.setInput(input);
		    byte[] result = profilePic.getBytes();
		    int resultLength = decompresser.inflate(result);
		    decompresser.end();

		    String output64 = new String(result, 0, resultLength, "UTF-8");*/
			
			if((passwordOnDB.equals(password)) && (isActiveUser == 1)) {
				if(newUser == 1) {
					responseParameterList.put("NewUser",true);
				}else if(newUser == 0) {
					responseParameterList.put("NewUser",false);
				}
				responseParameterList.put("Reason","Valid User");
				responseParameterList.put("UserAccess",userAccess);
				responseParameterList.put("FirstName",firstName);
				responseParameterList.put("MiddleName",secondName);
				responseParameterList.put("LastName",lastName);
				responseParameterList.put("EmployeeId",empId);
				//responseParameterList.put("ProfilePicBase64",output64);
				returnResponseObject.put("Success", true);
				returnResponseObject.put("ParameterList", responseParameterList);
				
				GenerateTimeStamp gt = new GenerateTimeStamp();
				loginTime = gt.getTimeStap();
				updateQuery = "update user set LAST_LOGIN=\"" + loginTime + "\" where (USERNAME='"+ userName +"'and ID <>0);";
				logger.info(updateQuery);
				DBConnect.updateData(con, updateQuery);
			}else if((passwordOnDB.equals(password)) && (isActiveUser != 1)){

				responseParameterList.put("Reason","You don't have access to use the System.");
				returnResponseObject.put("Success", false);
				returnResponseObject.put("ParameterList", responseParameterList);
				
			}else if(!passwordOnDB.equals(password)) {
				
				responseParameterList.put("Reason","UserName or Password is wrong, please try again.");
				returnResponseObject.put("Success", false);
				returnResponseObject.put("ParameterList", responseParameterList);
				
			}
			
			logger.info(returnResponseObject.toString());

			con.close();
			return returnResponseObject;

		} catch (Exception e) {
			
			if(e.toString().equals("java.sql.SQLException: Illegal operation on empty result set.")) {
				responseParameterList.put("Reason","UserName or Password is wrong, please try again.");
			}else if(e.toString().equals("java.lang.NullPointerException")){
				responseParameterList.put("Reason","UserName or Password is wrong, please try again.");
			}else {
				responseParameterList.put("Reason",e.toString());
			}
			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);
			con.close();
			logger.error(e);
			return returnResponseObject;
			
		}
		//return returnResponseObject;
	}
	
	
	@SuppressWarnings("unchecked")
	public JSONObject updateLogoutTime() throws Exception {
		JSONObject responseParameterList = new JSONObject();
		JSONObject returnResponseObject = new JSONObject();
		ConnectToDatabase DBConnect = new ConnectToDatabase();
		Connection con = DBConnect.getConnection();
		try {
			ResultSet rs = null;
			String updateQuery = null;
			String logoutTime = "";
			String userName = (String) this.loginParameterList.get("EmployeeCode");
			
			GenerateTimeStamp gt = new GenerateTimeStamp();
			logoutTime = gt.getTimeStap();
			updateQuery = "update user set LAST_LOGOUT=\"" + logoutTime + "\" where (USERNAME='"+ userName +"'and ID <>0);";
			logger.info(updateQuery);
			int res = DBConnect.updateData(con, updateQuery);
			if(res ==1) {
				responseParameterList.put("Reason","Logout Success, updated logout time on Server");
				returnResponseObject.put("Success", true);
				returnResponseObject.put("ParameterList", responseParameterList);
			}else {
				throw new Exception("Failed to update LogoutTime");
			}

			con.close();
			return returnResponseObject;

		} catch (Exception e) {
			
			responseParameterList.put("Reason",e.toString());
			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);
			con.close();
			logger.error(e);
			return returnResponseObject;
			
		}
		//return returnResponseObject;
	}
	

}
	
