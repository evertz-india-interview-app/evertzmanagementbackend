package evertz.evertz_management.application_server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.stream.Collectors;

import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;


public class EvertzManagementAppServer {

	public static String returnMessage;
	public static String portToListen;
	public static String apiEndPoint;
	public static String sqlServerIp;
	public static boolean serverStarted = false;
	public static JSONObject responseObject = new JSONObject();
	public static JSONObject reasonAndStatusObject = new JSONObject();

	public static void main(String[] args) {

		try {
			
			// Getting the Port and End point information from the SystemConfig file
			try {

				setLogProperties();

				sqlServerIp = SystemConfig.getSystemConfig("SQLServerIp");
				portToListen = SystemConfig.getSystemConfig("ServerListeningPort");
				apiEndPoint = SystemConfig.getSystemConfig("APIEndPoint");
			} catch (Exception e) {

				logger.error(e);
				System.exit(1);
			}
			
			System.out.println("Update system config in this file: " + SystemConfig.systemConfigLocation);
			System.out.println("------------------------------------------");
			System.out.println("Server listening to port: " + portToListen);
			System.out.println("------------------------------------------");
			System.out.println("Accepted Endpoint: /" + apiEndPoint);
			System.out.println("------------------------------------------");
			
			HttpServer server = HttpServer.create(new InetSocketAddress(Integer.parseInt(portToListen)), 0);
			HttpContext context = server.createContext("/" + apiEndPoint);
			context.setHandler(EvertzManagementAppServer::handleRequest);
			server.start();
			if(!serverStarted) {
				
				checkDBServerConnection();
				
			}
			

		} catch (Exception e) {

			logger.error(e);
			System.exit(1);
		}

	}
	
	public static void checkDBServerConnection() {
		
		try {
					
			HttpClient client = HttpClient.newHttpClient();
			String requestBody = "{\r\n" + 
					"      \"EvertzManagementApp\": {\r\n" + 
					"        \"Subsystem\": \"Server\",\r\n" + 
					"        \"Command\": \"ValidateServer\",\r\n" + 
					"        \"ParameterList\": {\r\n" + 
					"                \"ServerIp\": \""+ sqlServerIp +"\"\r\n" + 
					"            }\r\n" + 
					"        }\r\n" + 
					"    }";
			HttpRequest request = HttpRequest.newBuilder()
			        .uri(URI.create("http://"+ sqlServerIp +":" + portToListen + "/" + apiEndPoint))
			        .POST(HttpRequest.BodyPublishers.ofString(requestBody))
			        .build();
			HttpResponse<String> response = client.send(request,
			        HttpResponse.BodyHandlers.ofString());
			
			serverStarted = true;
			logger.info("server started");
			
		}catch (Exception e) {
			logger.error(e);
			System.exit(1);
		}
		
	}
	
	@SuppressWarnings({ "unchecked" })
	private static void handleRequest(HttpExchange exchange) throws IOException {

		exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		exchange.getResponseHeaders().add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
		exchange.getResponseHeaders().add("Access-Control-Allow-Headers",
				"X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding, X-Auth-Token, content-type, Authentication");

		try {

			printRequestInfo(exchange);

		} catch (Exception e) {

			reasonAndStatusObject.put("Success", false);
			reasonAndStatusObject.put("Reason", e.getMessage());
			responseObject.put("EvertzManagementApp", reasonAndStatusObject);

			returnMessage = responseObject.toString();
		}
		
		logger.info("Response: " + returnMessage);
		exchange.sendResponseHeaders(200, returnMessage.getBytes().length);
		OutputStream os = exchange.getResponseBody();
		os.write(returnMessage.getBytes());
		os.close();
	}
	
	private static void printRequestInfo(HttpExchange exchange) throws Exception {

		try {

			InputStreamReader isr;
			isr = new InputStreamReader(exchange.getRequestBody(), "utf-8");
			BufferedReader br = new BufferedReader(isr);
			String requestBodyString = br.lines().collect(Collectors.joining());
			
			if (requestBodyString.isEmpty()) {

				logger.info("Empty request received. Nothing to do.");
				
			} else {
				
				logger.info("Request: " + requestBodyString);
				
				JSONParser jsonParser = new JSONParser();

				JSONObject jsonRequestMessageObject = (JSONObject) jsonParser.parse(requestBodyString);

				JSONObject EvertzManagementApp = (JSONObject) jsonRequestMessageObject.get("EvertzManagementApp");

				String subsystemName = (String) EvertzManagementApp.get("Subsystem");
				String commandName = (String) EvertzManagementApp.get("Command");

				JSONObject parameterList = (JSONObject) EvertzManagementApp.get("ParameterList");

				// Subsystem, CommandName and ParameterList is mandatory for each APICall.
				// Validating and returning
				boolean validationResult = validateAPICall(subsystemName, commandName, parameterList);

				if (validationResult != true) {

					throw new Exception("Unknown error occurred");
				}

				DispatchToSubsystem dts = new DispatchToSubsystem(EvertzManagementApp);
				JSONObject responseFromSubsystem = dts.sendToRightHandler();

				returnMessage = responseFromSubsystem.toString();
			}

		} catch (Exception e) {

			logger.error(e);
			throw new Exception(e.getMessage());
		}
	}
	
	private static boolean validateAPICall(String subsystemName, String commandName, JSONObject parameterList)
			throws Exception {

		if (subsystemName.isEmpty() || subsystemName == null) {

			throw new Exception("Subsystem is empty or not provided");

		} else if (commandName.isEmpty() || commandName == null) {

			throw new Exception("CommandName is empty or not provided");

		} else if (parameterList.isEmpty() || parameterList == null) {

			throw new Exception("ParameterList is empty or not provided");

		}

		return true;

	}
    
	private static void setLogProperties() {

		try {

			String operatingSystem = SystemConfig.getSystemConfig("OperatingSystem");

			System.setProperty("logFileLoc", SystemConfig.getSystemConfig("LogFileLocation" + operatingSystem));

			String log4jConfPath = SystemConfig.getSystemConfig("Log4jPropertiesLocation");

			PropertyConfigurator.configure(log4jConfPath);

		} catch (Exception e) {

			logger.error(e);
			System.out.println("Unable to load logger properties. " + e.getMessage());
		}
	}

}
