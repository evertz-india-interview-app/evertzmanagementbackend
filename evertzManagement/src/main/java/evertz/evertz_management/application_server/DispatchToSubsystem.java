package evertz.evertz_management.application_server;

import org.json.simple.JSONObject;

import evertz.evertz_management.application_api_calls.Employee;
import evertz.evertz_management.application_api_calls.Function;
import evertz.evertz_management.application_api_calls.Login;
import evertz.evertz_management.application_api_calls.Profile;
import evertz.evertz_management.application_api_calls.UI;
import evertz.evertz_management.application_api_calls.ValidateServer;
public class DispatchToSubsystem {


	String subsystemName = null;
	String commandName = null;
	JSONObject parameterList = null;
	public DispatchToSubsystem(JSONObject requestFromClient) {
		
		this.subsystemName = (String) requestFromClient.get("Subsystem");
		this.commandName = (String) requestFromClient.get("Command");
		this.parameterList = (JSONObject) requestFromClient.get("ParameterList");
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject sendToRightHandler() throws Exception {

		// JSON parser object to parse read the response
		JSONObject returnFromHandler = new JSONObject();
		JSONObject responseObject = new JSONObject();

		try {

			if ((this.subsystemName.equals("Server")) && (this.commandName.equals("ValidateServer"))) {

				ValidateServer validate = new ValidateServer(parameterList);
				returnFromHandler = validate.validateServer();

			}else if ((this.subsystemName.equals("Login")) && (this.commandName.equals("Login"))) {

				Login login = new Login(parameterList);
				returnFromHandler = login.loginToServer();

			}else if ((this.subsystemName.equals("Login")) && (this.commandName.equals("Logout"))) {

				Login login = new Login(parameterList);
				returnFromHandler = login.updateLogoutTime();

			}else if((this.subsystemName.equals("UI")) && (this.commandName.equals("Get"))) {
				
				UI ui = new UI(parameterList);
				returnFromHandler = ui.getDropDownList();
				
			}else if((this.subsystemName.equals("Employee")) && (this.commandName.equals("AddEmployee"))) {
				
				Employee emp = new Employee(parameterList);
				returnFromHandler = emp.addEmployeeToServer();				
			}else if((this.subsystemName.equals("Function")) && (this.commandName.equals("Get"))) {
				
				Function fn = new Function(parameterList);
				returnFromHandler = fn.getTools();				
			}else if((this.subsystemName.equals("Profile")) && (this.commandName.equals("GetProfilePic"))) {
				
				Profile profile = new Profile(parameterList);
				returnFromHandler = profile.getProfilePic();				
			}else if((this.subsystemName.equals("Profile")) && (this.commandName.equals("UpdateProfilePic"))) {
				
				Profile profile = new Profile(parameterList);
				returnFromHandler = profile.updateProfilePic();				
			}
			
			
			returnFromHandler.put("Subsystem", this.subsystemName);
			returnFromHandler.put("Command", this.commandName);
			responseObject.put("EvertzManagementApp", returnFromHandler);

			return responseObject;

		} catch (Exception e) {

			logger.error(e);
			JSONObject responseParameterList = new JSONObject();
			JSONObject returnResponseObject = new JSONObject();
			responseParameterList.put("Reason",e.getMessage());
			returnResponseObject.put("Success", false);
			returnResponseObject.put("ParameterList", responseParameterList);
			responseObject.put("EvertzManagementApp", returnResponseObject);
			return responseObject;
		}
	}
}
