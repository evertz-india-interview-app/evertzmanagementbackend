package evertz.evertz_management.application_server;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class SystemConfig {
	static String currentDirectory = System.getProperty("user.dir");
	public static String systemConfigLocation = currentDirectory +"\\SystemConfig.json";
	public static File file;
	
public static String getSystemConfig(String configVariable) throws Exception {
		
		try {

			if (configVariable == "OperatingSystem") {
				String operatingSystem = System.getProperty("os.name");

				if (operatingSystem.startsWith("Windows")) {
					return "Windows";
				} else {
					return "Unix";
				}
			}

			file = new File(systemConfigLocation);
			
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(file));

			String configJson = br.lines().collect(Collectors.joining());

			JSONParser jsonParser = new JSONParser();
			String concatenatedJSONRequestMessage = "[" + configJson + "]";

			Object jsonRequestMessageObject = jsonParser.parse(concatenatedJSONRequestMessage);
			JSONArray jsonRequestMessageArray = (JSONArray) jsonRequestMessageObject;

			Iterator<?> iterator = jsonRequestMessageArray.iterator();

			while (iterator.hasNext()) {

				JSONObject JSONObjectIterator = (JSONObject) iterator.next();
				JSONObject EvertzManagementApp = (JSONObject) JSONObjectIterator.get("EvertzManagementApp");

				String configVariableValue = (String) EvertzManagementApp.get(configVariable);

				if (configVariableValue == null) {

					throw new IOException("System Config Value not found for: " + configVariable);
				}

				return configVariableValue;
			}

			throw new IOException("System Config Value not found for: " + configVariable);
		} catch (Exception e) {

			throw new Exception(e);
		}
	}

}
